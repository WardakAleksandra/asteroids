#pragma once
#include <atomic>
#include <set>
#include <chrono>
#include "meteor.h"


class MeteorLauncherData
{
public:
	const static unsigned int TIME_DELTA;
	const static unsigned int TIME_PERIOD_INIT;

	unsigned int getTimePeriod() const;
	void setTimePeriod(unsigned int);
	std::set<Meteor*> getMeteorSet() const;
	void setMeteorSet(const std::set<Meteor*>&);

	void insertMeteor(Meteor*);
	void deleteMeteor(Meteor*);
	void decreaseTimePeriod();
	
	MeteorLauncherData();
	~MeteorLauncherData();

private:
	std::atomic<unsigned int> time_period; //time between apperance of two meteors
	std::set<Meteor*> meteor_set;
	

};


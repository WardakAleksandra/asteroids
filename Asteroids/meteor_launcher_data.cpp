#include "meteor_launcher_data.h"

const unsigned int MeteorLauncherData::TIME_DELTA = 10;
const unsigned int MeteorLauncherData::TIME_PERIOD_INIT = 3000;

unsigned int MeteorLauncherData::getTimePeriod() const
{
	return time_period;
}

void MeteorLauncherData::setTimePeriod(unsigned int time_period_)
{
	time_period = time_period_;
}


std::set<Meteor*> MeteorLauncherData::getMeteorSet() const
{
	return meteor_set;
}

void MeteorLauncherData::setMeteorSet(const std::set<Meteor*>& meteor_set_)
{
	meteor_set = meteor_set_;
}

void MeteorLauncherData::insertMeteor(Meteor* meteor)
{
	meteor_set.insert(meteor);
}

void MeteorLauncherData::deleteMeteor(Meteor* meteor)
{
	meteor_set.erase(meteor);
	delete meteor;
}


void MeteorLauncherData::decreaseTimePeriod()
{
	time_period = getTimePeriod() + TIME_DELTA;
}

MeteorLauncherData::MeteorLauncherData() : time_period(TIME_PERIOD_INIT)
{

}


MeteorLauncherData::~MeteorLauncherData()
{
}

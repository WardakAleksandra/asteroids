#include "missile.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

const GLfloat vertices[] = {
	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, 0.5f, -0.5f,
	0.5f, 0.5f, -0.5f,
	-0.5f, 0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, -0.5f, 0.5f,
	0.5f, -0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, 0.5f,
	-0.5f, -0.5f, 0.5f,

	-0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, 0.5f,
	-0.5f, 0.5f, 0.5f,

	0.5f, 0.5f, 0.5f,
	0.5f, 0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,

	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, 0.5f,
	0.5f, -0.5f, 0.5f,
	-0.5f, -0.5f, 0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, 0.5f, -0.5f,
	0.5f, 0.5f, -0.5f,
	0.5f, 0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, -0.5f,
};


Missile::Missile(Coords spaceship_pos) : VisualElement()
{
	init();
	yOffset = spaceship_pos.y;
	position.x = spaceship_pos.x;
	position.y = yOffset;
	size.x = xScale;
	size.y = yScale;
}


Missile::~Missile()
{
}


void Missile::draw()
{
	shader->Use();
	glm::mat4 trans;
	calcOffset();
	position.y = yOffset;
	//	std::cout << params.xOffset << " " << params.yOffset <<  std::endl;
	trans = glm::translate(trans, glm::vec3(position.x, yOffset, -0.5f));
	trans = glm::scale(trans, glm::vec3(xScale, yScale, 1.0f));
	shader->setMat4("trans", trans);

	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void Missile::init()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STREAM_DRAW);

	// vertex geometry data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, NUMBER_OF_VERTICE_PARAMS * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	//for safety
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	VisualElement::shader = new ShaderProgram("missile.vert", "missile.frag");
}

void Missile::clean()
{
}

void Missile::calcOffset()
{
	yOffset += 0.0005f;
}
#include "game_engine.h"
#include <iostream>
#include <math.h>

GameEngine& GameEngine::getInstance()
{
	static GameEngine instance;
	return instance;
}

bool GameEngine::checkCollision(const VisualElement *one, const VisualElement* two)
{
	// Collision x-axis
	auto leftEdgeOne = one->position.x - one->size.x / 2;
	auto rightEdgeOne = one->position.x + one->size.x / 2;

	auto leftEdgeTwo = two->position.x - two->size.x / 2;
	auto rightEdgeTwo = two->position.x + two->size.x / 2;

	bool collisionX = (leftEdgeTwo < rightEdgeOne && leftEdgeTwo > leftEdgeOne) ||
		(rightEdgeTwo < rightEdgeOne && rightEdgeTwo > leftEdgeOne);

	// Collision y-axis
	auto topOne = one->position.y - one->size.y / 2;
	auto bottomOne = one->position.y + one->size.y / 2;

	auto topTwo = two->position.y - two->size.y / 2;
	auto bottomTwo = two->position.y + two->size.y / 2;

	bool collisionY = (topTwo < bottomOne && topTwo > topOne) ||
		(bottomTwo < bottomOne && bottomTwo > topOne);

	return collisionX && collisionY;
}

bool GameEngine::isVisible(const VisualElement* obj)
{
	float diagonal = sqrt(pow(obj->size.x, 2) + pow(obj->size.y, 2));

	auto leftEdge = obj->position.x - diagonal / 2;
	auto rightEdge = obj->position.x + diagonal / 2;
	auto topEdge = obj->position.y - diagonal / 2;
	auto bottomEdge = obj->position.y + diagonal / 2;

	//std::cout << " L: " << leftEdge << " R: " << rightEdge << " T: " << topEdge << " B: " << bottomEdge << std::endl;
	return !(leftEdge < -1.5f || rightEdge > 1.5f || topEdge < -1.5f || bottomEdge > 1.5f);

}

GameEngine::GameEngine()
{
}


GameEngine::~GameEngine()
{
}

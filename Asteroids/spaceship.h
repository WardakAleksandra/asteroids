#pragma once
#include "visual_element.h"

class Spaceship : public VisualElement
{
public:
	Spaceship();
	virtual ~Spaceship();
	void draw();
	void clean();
private:
	static const short NUMBER_OF_VERTICES = 36;
	static const short NUMBER_OF_VERTICE_PARAMS = 3;
	volatile float xOffset = 0.0f;
	const float xScale = 0.2f;
	void init();
	
};


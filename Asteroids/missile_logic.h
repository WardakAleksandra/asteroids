#pragma once
class MissileLogic
{
public:
	static MissileLogic& getInstance();
	void calcOffset(MissileLogic&) const;
private:
	MissileLogic(const MissileLogic&) = delete;
	MissileLogic& operator=(const MissileLogic&) = delete;
	MissileLogic();
	~MissileLogic();
};


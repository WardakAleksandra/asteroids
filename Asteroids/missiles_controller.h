#pragma once
#include <atomic>
#include <set>
#include "missile.h"
#include "meteor.h"

class MissilesController
{
public:
	static MissilesController& getInstance();
	void startFiring();
	void createMissileIfNeeded(Coords);
	void drawMissiles();
	void deleteMeteorIfNeeded();
	void checkCollision();

private:
	std::atomic<bool> createMissileFlag = false;
	void setFlag();
	const static unsigned int TIME_PERIOD; //time between apperance of two missiles
	std::set<Missile*> missiles_set;

	MissilesController(const MissilesController&) = delete;
	MissilesController& operator=(const MissilesController&) = delete;
	MissilesController();
	~MissilesController();
};


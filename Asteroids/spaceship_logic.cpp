#include "spaceship_logic.h"
#include "key_dispatcher.h"

SpaceshipLogic& SpaceshipLogic::getInstance()
{
	static SpaceshipLogic instance;
	return instance;
}


float SpaceshipLogic::calcXOffset(float xOffset, float xScale)
{
	float newOffset = xOffset;
	float speed = 0.0006f;

	if (KeyDispatcher::getInstance().keys_state[GLFW_KEY_LEFT] == true)
	{
		newOffset -= speed;
		if (newOffset <= -1.0f + xScale / 2)
			newOffset = -1.0f + xScale / 2;
	}
	else if (KeyDispatcher::getInstance().keys_state[GLFW_KEY_RIGHT] == true)
	{
		newOffset += speed;
		if (newOffset >= 1.0f - xScale / 2)
			newOffset = 1.0f - xScale / 2;
	}
	return newOffset;
}
SpaceshipLogic::SpaceshipLogic()
{
}


SpaceshipLogic::~SpaceshipLogic()
{
}

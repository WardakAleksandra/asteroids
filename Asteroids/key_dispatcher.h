#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <map>
#include <atomic>

class KeyDispatcher
{
public:
	static KeyDispatcher& getInstance();
	static void keyCallback(GLFWwindow*, int, int, int, int);
	void keyCallbackImpl(GLFWwindow*, int, int, int, int);
	std::map<int, std::atomic<bool> > keys_state;
private:


	KeyDispatcher();
	KeyDispatcher(const KeyDispatcher&) = delete;
	KeyDispatcher& operator=(const KeyDispatcher&) = delete;
	~KeyDispatcher();
};


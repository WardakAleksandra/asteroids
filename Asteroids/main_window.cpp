#include "main_window.h"
#include "general_utils.h"
#include "key_dispatcher.h"
#include <iostream>
#include <cassert>

MainWindow& MainWindow::getInstance() {
	static MainWindow instance;
	return instance;
}


GLFWwindow* MainWindow::init(){

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	try
	{
		GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ASTEROIDS", nullptr, nullptr);
		if (window == nullptr)
			throw std::exception("GLFW window not created");
		glfwMakeContextCurrent(window);
		glfwSetKeyCallback(window, &KeyDispatcher::keyCallback);
		general_utils::glCheckError();

		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
			throw std::exception("GLEW Initialization failed");
		general_utils::glCheckError();

		glViewport(0, 0, WIDTH, HEIGHT);
		return window;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
	}

}

MainWindow::MainWindow()
{
}


MainWindow::~MainWindow()
{
}

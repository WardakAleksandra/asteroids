#include <GL/glew.h>
#define glCheckError() glCheckError_(__FILE__, __LINE__) 
namespace general_utils
{
	extern GLenum glCheckError_(const char *file, int line);
	extern float randFloat(const float min, const float max);
}
	
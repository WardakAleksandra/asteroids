#pragma once
class SpaceshipLogic
{
public:
	static SpaceshipLogic& getInstance();
	float calcXOffset(float, float);

private:
	SpaceshipLogic(const SpaceshipLogic&) = delete;
	SpaceshipLogic& operator=(const SpaceshipLogic&) = delete;
	SpaceshipLogic();
	~SpaceshipLogic();
};


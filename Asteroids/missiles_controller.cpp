#include "missiles_controller.h"
#include "game_engine.h"
#include <iostream>
#include "meteor_launcher_controller.h"

const unsigned int MissilesController::TIME_PERIOD = 300;

MissilesController& MissilesController::getInstance()
{
	static MissilesController instance;
	return instance;
}

void MissilesController::startFiring()
{
	setFlag();
}


void MissilesController::createMissileIfNeeded(Coords pos)
{
	if (createMissileFlag)
	{
		createMissileFlag = false;
		missiles_set.insert(new Missile(pos));		
	}
}
void MissilesController::setFlag()
{
	createMissileFlag = true;
}

void MissilesController::drawMissiles()
{
	for (const auto& missile : missiles_set) {
		missile->draw();
	}
}

void MissilesController::deleteMeteorIfNeeded()
{
	for (auto missile : missiles_set)
	{
		if (!GameEngine::getInstance().isVisible(missile))
		{
			std::cout << "Missile dissapear" << std::endl;
			missiles_set.erase(missile);
			delete missile;
		}
	}
}

void MissilesController::checkCollision()
{
	for (auto& missile : missiles_set)
	{
		if (MeteorLauncherController::getInstance().checkCollision(missile))
		{
			missiles_set.erase(missile);
			delete missile;
		}
	}
}

MissilesController::MissilesController() 
{
}


MissilesController::~MissilesController()
{
}

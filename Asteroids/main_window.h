#pragma once
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class MainWindow
{
public:
	static MainWindow& getInstance();
	GLFWwindow* init();
	static const GLuint WIDTH = 800, HEIGHT = 600;

private:
	MainWindow();
	MainWindow(const MainWindow&) = delete;
	MainWindow& operator=(const MainWindow&) = delete;
	~MainWindow();
};


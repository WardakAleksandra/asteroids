#pragma once
#include "visual_element.h"

struct MeteorParams {
	volatile float xOffset = 0.0f;
	volatile float yOffset = 0.0f;
	float xScale;
	float yScale;
	float xOffsetDelta;
	float rotSpeed;
};

class Meteor : public VisualElement
{
public:
	Meteor();
	virtual ~Meteor();
	void draw();
	void clean();
private:
	static const short NUMBER_OF_VERTICES = 36;
	static const short NUMBER_OF_VERTICE_PARAMS = 3;
	MeteorParams params;
	float initTime;
	void init();
};


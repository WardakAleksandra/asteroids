#pragma once
#include "visual_element.h"
#include <memory>

class GameEngine
{
public:
	static GameEngine& getInstance();
	bool checkCollision(const VisualElement*, const VisualElement*);
	bool isVisible(const VisualElement*);
private:

	GameEngine(const GameEngine&) = delete;
	GameEngine& operator=(const GameEngine&) = delete;
	GameEngine();
	~GameEngine();
};


#include "meteor_launcher_controller.h"
#include "meteor_launcher_timer.h"
#include <future>
#include "game_engine.h"
#include <iostream>

MeteorLauncherController& MeteorLauncherController::getInstance()
{
	static MeteorLauncherController instance;
	return instance;
}

void MeteorLauncherController::decreaseTimePeriod()
{
	meteorLauncherData->decreaseTimePeriod();
}

void MeteorLauncherController::createMeteor()
{
	meteorLauncherData->insertMeteor(new Meteor());
}

void MeteorLauncherController::drawMeteors()
{
	for (const auto& meteor : meteorLauncherData->getMeteorSet()) {
		meteor->draw();
	}
}


void MeteorLauncherController::startMeteorLauncher()
{
	std::unique_ptr<MeteorLauncherTimer> timer(new MeteorLauncherTimer);
	timer->timer_start(std::bind(&MeteorLauncherController::setFlag, this), meteorLauncherData->getTimePeriod());
}

void MeteorLauncherController::setFlag()
{
	createMeteorFlag = true;
}

void MeteorLauncherController::createMeteorIfNeeded()
{
	if (createMeteorFlag)
	{
		createMeteorFlag = false;
		createMeteor();
	}
}

void MeteorLauncherController::deleteMeteorIfNeeded()
{
	for (auto meteor : meteorLauncherData->getMeteorSet())
	{
		if (!GameEngine::getInstance().isVisible(meteor))
		{
			std::cout << "Meteor dissapear" << std::endl;
			deleteMeteor(meteor);
		}
	}
}

bool MeteorLauncherController::checkCollision(VisualElement* obj)
{
	for (auto meteor : meteorLauncherData->getMeteorSet())
	{
		if (GameEngine::getInstance().checkCollision(meteor, obj))
		{
			deleteMeteor(meteor);			
			return true;
		}
	}
	return false;
}

void MeteorLauncherController::deleteMeteor(Meteor* meteor)
{
	meteorLauncherData->deleteMeteor(meteor);
}

MeteorLauncherController::MeteorLauncherController()
{
	meteorLauncherData = std::unique_ptr<MeteorLauncherData>(new MeteorLauncherData);
}


MeteorLauncherController::~MeteorLauncherController()
{
}

/*This class define abstract visual element in OpenGL*/

#pragma once
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "shader_program.h"

struct Coords{
	float x;
	float y;
};

class VisualElement
{
public:
	Coords position;
	Coords size;

	virtual void draw() = 0;
	virtual void clean();
	Coords getPosition() const;
	Coords getSize() const;

	VisualElement();
	virtual ~VisualElement();

protected:
	GLuint VAO, VBO;
	ShaderProgram* shader;
	virtual void init() = 0;
	Coords& operator=(const Coords&) = delete;
};


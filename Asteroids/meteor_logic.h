#pragma once

struct MeteorParams;

class MeteorLogic
{
public:
	static MeteorLogic& getInstance();
	void calcOffset(MeteorParams&) const;
	void initMateorParams(MeteorParams&) const;
private:
	MeteorLogic(const MeteorLogic&) = delete;
	MeteorLogic& operator=(const MeteorLogic&) = delete;
	MeteorLogic();
	~MeteorLogic();
};


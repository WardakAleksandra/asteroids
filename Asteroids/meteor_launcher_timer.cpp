#include "meteor_launcher_timer.h"
#include <chrono>
#include <thread>
#include <iostream>



void MeteorLauncherTimer::timer_start(std::function<void(void)> func, unsigned int interval)
{
	std::thread([func, interval]()
	{
		while (true)
		{
			auto x = std::chrono::steady_clock::now() + std::chrono::milliseconds(interval);
			func();
			std::this_thread::sleep_until(x);
		}
	}).detach();
}


MeteorLauncherTimer::MeteorLauncherTimer()
{
}


MeteorLauncherTimer::~MeteorLauncherTimer()
{
}

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "spaceship.h"
#include "general_utils.h"
#include "main_window.h"
#include "spaceship_logic.h"


const GLfloat vertices[] = {
	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f, 
	0.5f, 0.5f, -0.5f, 
	0.5f, 0.5f, -0.5f, 
	-0.5f, 0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, -0.5f, 0.5f,
	0.5f, -0.5f, 0.5f, 
	0.5f, 0.5f, 0.5f, 
	0.5f, 0.5f, 0.5f, 
	-0.5f, 0.5f, 0.5f, 
	-0.5f, -0.5f, 0.5f,

	-0.5f, 0.5f, 0.5f, 
	-0.5f, 0.5f, -0.5f, 
	-0.5f, -0.5f, -0.5f, 
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, 0.5f, 
	-0.5f, 0.5f, 0.5f,

	0.5f, 0.5f, 0.5f, 
	0.5f, 0.5f, -0.5f, 
	0.5f, -0.5f, -0.5f, 
	0.5f, -0.5f, -0.5f, 
	0.5f, -0.5f, 0.5f, 
	0.5f, 0.5f, 0.5f, 

	-0.5f, -0.5f, -0.5f, 
	0.5f, -0.5f, -0.5f, 
	0.5f, -0.5f, 0.5f, 
	0.5f, -0.5f, 0.5f, 
	-0.5f, -0.5f, 0.5f, 
	-0.5f, -0.5f, -0.5f, 

	-0.5f, 0.5f, -0.5f, 
	0.5f, 0.5f, -0.5f, 
	0.5f, 0.5f, 0.5f, 
	0.5f, 0.5f, 0.5f, 
	-0.5f, 0.5f, 0.5f, 
	-0.5f, 0.5f, -0.5f,
};

Spaceship::Spaceship() : VisualElement()
{
	init();
	position.x = 0.0f;
	position.y = -0.85f;
	size.x = xScale;
	size.y = 0.3f;
}


Spaceship::~Spaceship()
{

}

void Spaceship::draw()
{
	shader->Use();
	glm::mat4 trans;
	xOffset = SpaceshipLogic::getInstance().calcXOffset(xOffset, xScale);
	position.x = xOffset;
//	std::cout << xOffset << std::endl;
	trans = glm::translate(trans, glm::vec3(xOffset, -0.85f, -0.5f));
	trans = glm::scale(trans, glm::vec3(xScale, 0.3f, 1.0f));
	shader->setMat4("trans", trans);
	
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	general_utils::glCheckError();
}


void Spaceship::init()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

	// vertex geometry data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, NUMBER_OF_VERTICE_PARAMS * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	//for safety
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	VisualElement::shader = new ShaderProgram("spaceship.vert", "spaceship.frag");
	general_utils::glCheckError();
}

void Spaceship::clean()
{
}
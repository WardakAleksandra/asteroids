#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "general_utils.h"
#include "meteor.h"
#include "meteor_logic.h"

const GLfloat vertices[] = {
	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, 0.5f, -0.5f,
	0.5f, 0.5f, -0.5f,
	-0.5f, 0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, -0.5f, 0.5f,
	0.5f, -0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, 0.5f,
	-0.5f, -0.5f, 0.5f,

	-0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, 0.5f,
	-0.5f, 0.5f, 0.5f,

	0.5f, 0.5f, 0.5f,
	0.5f, 0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,

	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, 0.5f,
	0.5f, -0.5f, 0.5f,
	-0.5f, -0.5f, 0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, 0.5f, -0.5f,
	0.5f, 0.5f, -0.5f,
	0.5f, 0.5f, 0.5f,
	0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, 0.5f,
	-0.5f, 0.5f, -0.5f,
};

Meteor::Meteor() : VisualElement()
{
	MeteorLogic::getInstance().initMateorParams(params);
	
	//initTime = (float)glfwGetTime();

	init();
	position.x = params.xOffset;
	position.y = params.yOffset;
	size.x = params.xScale;
	size.y = params.yScale;
}

Meteor::~Meteor()
{
}

void Meteor::draw()
{
	shader->Use();
	glm::mat4 trans;
	MeteorLogic::getInstance().calcOffset(params);
	position.x = params.xOffset;
	position.y = params.yOffset;
//	std::cout << params.xOffset << " " << params.yOffset <<  std::endl;
	trans = glm::translate(trans, glm::vec3(params.xOffset, params.yOffset, -0.5f));
	trans = glm::scale(trans, glm::vec3(params.xScale, params.yScale, 1.0f));
	trans = glm::rotate(trans, (float)glfwGetTime() * glm::radians(params.rotSpeed), glm::vec3(0.0f, 0.0f, 1.0f));
	shader->setMat4("trans", trans);

	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	general_utils::glCheckError();
}

void Meteor::init()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STREAM_DRAW);

	// vertex geometry data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, NUMBER_OF_VERTICE_PARAMS * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	//for safety
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	VisualElement::shader = new ShaderProgram("meteor.vert", "meteor.frag");
	general_utils::glCheckError();
}

void Meteor::clean()
{
}
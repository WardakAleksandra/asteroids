#pragma once
#include <memory>
#include "meteor_launcher_data.h"
#include <condition_variable>
#include <mutex>

class MeteorLauncherController
{
public:
	static MeteorLauncherController& getInstance();
	void decreaseTimePeriod();
	void drawMeteors();

	void startMeteorLauncher();
	void createMeteorIfNeeded();
	void deleteMeteorIfNeeded();
	bool checkCollision(VisualElement*);
	//TODO switch to private
	void createMeteor();
	void deleteMeteor(Meteor*);
private:
	std::unique_ptr<MeteorLauncherData> meteorLauncherData;
	std::atomic<bool> createMeteorFlag = false;

	void setFlag();

	MeteorLauncherController();
	MeteorLauncherController(const MeteorLauncherController&) = delete;
	MeteorLauncherController& operator=(const MeteorLauncherController&) = delete;
	~MeteorLauncherController();
};


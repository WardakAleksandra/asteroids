#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <memory>
#include "main_window.h"
#include "spaceship.h"
#include "meteor.h"
#include "general_utils.h"
#include "meteor_launcher_controller.h"
#include "missiles_controller.h"

int main()
{
	glewExperimental = GL_TRUE;
	if (glfwInit() != GL_TRUE)
	{
		std::cout << "GLFW initialization failed" << std::endl;
		return -1;
	}
	
	MeteorLauncherController& meteorController = MeteorLauncherController::getInstance();
	MissilesController& missilesController = MissilesController::getInstance();

	try
	{		
		GLFWwindow* window = MainWindow::getInstance().init();
		general_utils::glCheckError();

		Spaceship *spaceship = new Spaceship();
		general_utils::glCheckError();
		meteorController.startMeteorLauncher();
		missilesController.startFiring();

		// main loop
		while (!glfwWindowShouldClose(window))
		{
			meteorController.createMeteorIfNeeded();
			missilesController.createMissileIfNeeded(spaceship->getPosition());
				
			// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
			glfwPollEvents();

			// Clear the colorbuffer
			glClearColor(0.05f, 0.05f, 0.15f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			spaceship->draw();
			meteorController.drawMeteors();
			missilesController.drawMissiles();

			glfwSwapBuffers(window);
			general_utils::glCheckError();

			if (meteorController.checkCollision(spaceship))
			{
				//TODO: reset game
			}
			meteorController.deleteMeteorIfNeeded();
		//	missilesController.checkCollision();
	//		missilesController.deleteMeteorIfNeeded();
		}
		spaceship->clean();
		delete spaceship;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
	}

	glfwTerminate();

	return 0;
}
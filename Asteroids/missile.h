#pragma once
#include "visual_element.h"

class Missile : public VisualElement
{
public:
	Missile(Coords);
	~Missile();
	void draw();
	void clean();
private:
	static const short NUMBER_OF_VERTICES = 36;
	static const short NUMBER_OF_VERTICE_PARAMS = 3;
	void init();
	void calcOffset();
	float yOffset;
	float xScale = 0.01f;
	float yScale = 0.05f;
};


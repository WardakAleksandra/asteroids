#include "key_dispatcher.h"
#include "missiles_controller.h"
#include <iostream>

void KeyDispatcher::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	getInstance().keyCallbackImpl(window, key, scancode, action, mode);
}

void KeyDispatcher::keyCallbackImpl(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	std::cout << key << std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	else if (action == GLFW_PRESS)
	{
		//TODO: check if any other key is pressed

		keys_state[key] = true;
		if (key == GLFW_KEY_UP)
		{
			MissilesController::getInstance().startFiring();
		}
	}
	else if (action == GLFW_RELEASE)
		keys_state[key] = false;
}

KeyDispatcher& KeyDispatcher::getInstance()
{
	static KeyDispatcher instance;
	return instance;
}

KeyDispatcher::KeyDispatcher()
{
	keys_state[GLFW_KEY_LEFT] = false;
	keys_state[GLFW_KEY_RIGHT] = false;
	keys_state[GLFW_KEY_UP] = false;
}


KeyDispatcher::~KeyDispatcher()
{
}


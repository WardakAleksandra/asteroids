#include "visual_element.h"


VisualElement::VisualElement()
{
}


VisualElement::~VisualElement()
{
}


void VisualElement::clean()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	delete shader;
}

Coords VisualElement::getPosition() const
{
	return position;
}

Coords VisualElement::getSize() const
{
	return size;
}
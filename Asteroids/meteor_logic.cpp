#include "meteor_logic.h"
#include "meteor.h"
#include "general_utils.h"
#include <iostream>

MeteorLogic& MeteorLogic::getInstance()
{
	static MeteorLogic instance;
	return instance;
}

void MeteorLogic::initMateorParams(MeteorParams& params) const
{
	params.xScale = general_utils::randFloat(0.05f, 0.3f);
	params.yScale = general_utils::randFloat(0.05f, 0.3f);
	params.xOffset = general_utils::randFloat(-1.0f + params.xScale, 1.0f - params.xScale); // not to start from the very edge
	params.yOffset = 1.0f;
	params.xOffsetDelta = general_utils::randFloat(-0.00001f, 0.00001f);
	params.rotSpeed = general_utils::randFloat(-100.0f, 100.0f);
	std::cout << params.xScale << " " << params.yScale << std::endl;
}

void MeteorLogic::calcOffset(MeteorParams& params) const
{
	float xSpeed = params.xOffsetDelta;
	float ySpeed = 0.0005f;

	params.xOffset -= xSpeed;
	params.yOffset -= ySpeed;

}

MeteorLogic::MeteorLogic()
{
}


MeteorLogic::~MeteorLogic()
{
}
